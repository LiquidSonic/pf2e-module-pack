# PF2E Module Pack

This project houses my modules for community use. This is my first time scripting & making modules so heres hoping everything goes smoothly!

So far all thats included is the Condition Setter & StatusCheck Macros which must both be used together to operate correctly. It requires the modules Turn Alert & Furnace to behave correctly. 

Please use this manifest link : https://gitlab.com/LiquidSonic/pf2e-module-pack/-/raw/master/module.json to install this.

Please report any issues to the Issues area.

<h1> Condition Setter </h1>

<div align="center"> ![image.png](./image.png) </div>

My first every project in Javascript and what makes me appreciate all the coders out there who toil their asses away at the screen!! This is by far my biggest project to date. For this to function properly please drag both "Condition Setter" AND "StatusCheck" from the compendium into your games. The name of "StatusCheck" should not change and should be the only macro called StatusCheck in your games!!


<div align="center"> ![image](/uploads/09f0a3b5e27c398a2cdddca8a3067b5e/image.png) </div>

For the condition setter to work you must first have an active combat, the token you've selected or targeted must be an active combatant in the combat.

Once you've done this you'll get the pop up you see above. Here you can set the amount of rounds and value of the condition as well as whether the condition should resolve it solve at the end of the turn, or beginning of the turn. Please keep in mind that Persistent damage and Frightened will be set to the end of the turns unless otherwise stated for some special circumstance.

<hr>

<div align="center"> ![image](/uploads/57d39e9bcad870c83a61d412ad36cdd9/image.png)  ![image](/uploads/b3a6157341418f37a5af89b5c2b0e6d3/image.png) </div>




Special Thanks goes out to Atrophos, Nerull, Nikolaj-a and Ustin in helping make this all possible! (and those people who I picked code out of their own macros to help build this one!)


<h1> Counteract </h1>

This module is a relatively simple one for giving you a box to handle DC levels, counteract levels of the "Counteract" mechanic subsystem. I found it a fairly complicated thing to explain to players and to wrap my head around and decided to make a macro that broke it down.
<div align="center">  ![image](/uploads/8c1111666c51bd14fd1f0fc43a0c5955/image.png) </div>

Most of how to use it is fairly self explanatory and should make sense on how to use and will print out a nifty output if you were successful or not and by what degree.

<div align="center"> ![image](/uploads/b653a3d1d9acd7b1de1ef847b7e0b3b5/image.png) </div>

